package at.spenger.xml.shiporder;

import javax.xml.bind.annotation.XmlElement;

public class Traktor {
	
    @XmlElement(required = true)
    protected String land;
    protected String typ;
    protected String jahr;
    protected String wert;
	public String getLand() {
		return land;
	}
	public void setLand(String land) {
		this.land = land;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getJahr() {
		return jahr;
	}
	public void setJahr(String jahr) {
		this.jahr = jahr;
	}
	public String getWert() {
		return wert;
	}
	public void setWert(String wert) {
		this.wert = wert;
	}
	@Override
	public String toString() {
		return "Traktor [land=" + land + ", typ=" + typ + ", jahr=" + jahr
				+ ", wert=" + wert + "]";
	}
    

}
